package com.example.other

enum class ItemName(var itemName:String , var itemPrice:Int) {

    blackTea("紅茶",130),

    greenTea("綠茶",130),

    doudouGreenTea("多多綠茶",130),

    charcoalRoastedOolongTea("碳培烏龍茶",130),

    osmanthusOolongTea("冰釀桂花烏龍茶",130),

    osmanthusEbonyBlackTea("桂花烏梅紅茶",130),

    osmanthusEbonyGreenTea("桂花烏梅綠茶",130),

    passionFruitBlackTea("百香紅茶",130),

    passionFruitGreenTea("百香綠茶",130),

    lemonBlackTea("檸檬紅茶",130),

    lemonGreenTea("檸檬綠茶",130)

}
