package com.example.possystem

import android.content.DialogInterface
import android.content.pm.ActivityInfo
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.ComputableLiveData
import com.example.other.ItemName
import kotlin.collections.ArrayList

class MenuActivity : AppCompatActivity() {

    var itemButtons: List<Button> = ArrayList()

    var orderItemName : String = String()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
        setContentView(R.layout.activity_menu)

        orderItemName = null.toString()

        var menuView = findViewById<LinearLayout>(R.id.menuView)

        for (itemName in ItemName.values()) {

            var itemButton = Button(this)

            itemButton.text = itemName.itemName

            itemButtons += itemButton

        }

        for (i in itemButtons.indices) {

            itemButtons[i].id = i

            menuView.addView(itemButtons[i])

        }

        for (i in itemButtons.indices)

            itemButtons[i].setOnClickListener{

                orderItemName = itemButtons[i].text.toString()

                orderItem()

            }

    }

    fun orderItem() {

        var dialogView = LayoutInflater.from(this).inflate(R.layout.dialog,null)

        var builder = AlertDialog.Builder(this)

            .setView(dialogView)

        var dialog = builder.show()

        var iceCheckGroup = dialog.findViewById<RadioGroup>(R.id.iceCheckGroup)

        var sweetLevelCheckGroup = dialog.findViewById<RadioGroup>(R.id.sweetLevelCheckGroup)

        var temp : String = null.toString()

        iceCheckGroup!!.setOnCheckedChangeListener {

                view, _ ->

            var radioButtonTemp = dialog.findViewById<RadioButton>(view.id)

            when(view.id){

                R.id.moreIce -> temp = radioButtonTemp?.text.toString()

                R.id.lessIce -> temp = radioButtonTemp?.text.toString()

                R.id.iceFree -> temp = radioButtonTemp?.text.toString()

            }

        }

        sweetLevelCheckGroup?.setOnCheckedChangeListener {

                view, _ ->

            var radioButtonTemp = dialog.findViewById<RadioButton>(view.id)

            when(view.id){

                R.id.fullSugar -> temp = temp + " " + radioButtonTemp?.text.toString()

                R.id.lessSugar -> temp = temp + " " + radioButtonTemp?.text.toString()

                R.id.halfSugar -> temp = temp + " " + radioButtonTemp?.text.toString()

                R.id.quarterSugar -> temp = temp + " " + radioButtonTemp?.text.toString()

                R.id.sugarFree -> temp = temp + " " + radioButtonTemp?.text.toString()

            }

        }
//
        var ButtonTemp : Button? = dialog.findViewById<Button>(R.id.checkButton)

        ButtonTemp!!.setOnClickListener {

            orderItemName =  orderItemName + " " + temp

            dialog.dismiss()

        }

        Log.i("test ：",orderItemName)

    }

}



